#include "mod_proc.h"

ssize_t proc_node_read(char *buffer, char **start, off_t off,
	int count, int *eof, void *data )
{
	static int offset = 0, i;
	printk(KERN_INFO "read: %d\n", count);
	for (i = 0; offset <= LEN_MSG && '\0' != buf_msg[ offset ]; offset++, i++) {
		*( buffer + i ) = buf_msg[ offset ];
	}
	*( buffer + i ) = '\n';
	i++;
	if (offset >= LEN_MSG || '\0' == buf_msg[ offset ]) {
		offset = 0;
	}
	printk(KERN_INFO "return bytes: %d\n", i);
	return i;
};

static int __init proc_init(void)
{
	int ret;
	struct proc_dir_entry *own_proc_node;
	own_proc_node = create_proc_entry(NAME_NODE, S_IFREG | S_IRUGO | S_IWUGO, NULL);
	if (NULL == own_proc_node) {
		ret = -ENOMEM;
		printk(KERN_ERR "can't create /proc/%s\n", NAME_NODE);
		goto err_node;
	}
	own_proc_node->uid = 0;
	own_proc_node->gid = 0;
	own_proc_node->read_proc = proc_node_read;
	printk(KERN_INFO "module : loaded\n");
	return 0;
err_node:
	return ret;
}

static void __exit proc_exit(void)
{
	remove_proc_entry(NAME_NODE, NULL);
	printk(KERN_INFO "/proc/%s removed\n", NAME_NODE);
	printk(KERN_INFO "module: unloaded\n");
}

