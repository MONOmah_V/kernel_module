#include <linux/module.h>
#include <linux/proc_fs.h>
/*#include <linux/stat.h>*/
/*#include <asm/uaccess.h>*/

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ilya Vassyutovich <MONOmah_V@mail.ru>" );

static int  __init proc_init(void);
static void __exit proc_exit(void);

module_init(proc_init);
module_exit(proc_exit); 

#define NAME_NODE "mod_node"

#define LEN_MSG 160
static char buf_msg[LEN_MSG + 1] = "Hello from module!";

